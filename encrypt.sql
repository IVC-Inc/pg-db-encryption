
create or replace function rm_perm_views() returns void language plpgsql as $$

declare
	rec record;
begin
	for rec in 	select relname 
				from pg_class 
				where relkind = 'v' and
					relname ~ $_$^madv[0-9]+_org_$_$
				
	loop
		execute 'drop view ' || rec.relname;
	end loop;
	for rec in 	select relname 
				from pg_class 
				where relkind = 'v' and
					relname ~ $_$^m[0-9]+_org_$_$
				
	loop
		execute 'drop view ' || rec.relname;
	end loop;
	return;
end;

$$;

select rm_perm_views();


create or replace function master_key () returns bytea language plperlu as $$

# uses a cache so if you change the master key restart the DB
 
unless ( exists $_SHARED{master_key} )
{
   my $fh;
   open($fh, "/opt/EnterpriseDB/config/config.dat") or die "error opening master key file: $!";
   my $line=<$fh>;
   close($fh);
   chomp $line;
   die "master key too short" unless length($line) > 20;
   # turn hex digits into raw bytes
   my $esckey = pack('H*',$line);
   # then escape the raw bytes into a bytea literal string
   # need to escape $ here so we avoid blundering into dollar quotes 
   # when used in url_key
   $esckey =~ s!(\\|\$|[^ -~])!sprintf("\\%03o",ord($1))!ge;
   $_SHARED{master_key} = $esckey;
}

return  $_SHARED{master_key};

$$;

alter table merit_accounts add key bytea;

create or replace function gen_url_key (text) returns void 
language plperlu as $f$


my $url = shift;
my $fh;
open($fh,"/dev/urandom");
my $bytes;
sysread($fh,$bytes,16);
close($fh);
my $esckey = $bytes;
# need to escape $ here so we avoid blundering into dollar quotes
$esckey =~ s!(\\|\$|[^ -~])!sprintf("\\%03o",ord($1))!ge; 
my $rv = spi_exec_query("update merit_accounts
                         set key = encrypt(\$\$$esckey\$\$,
                                            master_key(),'aes')
                         where url = '$url'");
return undef;

$f$;

create or replace function gen_all_url_keys() returns void language plpgsql 
as $$

declare
        rec record;
begin
        for rec in select url,key from merit_accounts
        loop
                if rec.key is null then
                        perform gen_url_key(rec.url);
                end if;
        end loop;
		return;
end;

$$;

select gen_all_url_keys();


-- function to fetch the (decoded) url key
create or replace function url_key(text) returns bytea language plperlu as $f$

# uses a cache so if a key gets changed probably best to restart the DB
# adding a new key is fine

my $url = shift;
unless (exists $_SHARED{urlkeys}{$url})
{
	spi_exec_query("select master_key()") 
		unless (exists $_SHARED{master_key});
	my $mk = $_SHARED{master_key};
	my $rv = spi_exec_query("select decrypt(key,\$\$$mk\$\$,'aes')
                                 as key
                             from merit_accounts
                             where url = '$url'");
    die "no key found for $url!" unless $rv->{processed} == 1;
	# no need to do escaping here - the plperl interface has already
	# done it for us.
    my $clearkey = $rv->{rows}[0]->{key};
	$_SHARED{urlkeys}{$url} = $clearkey;	
}

return $_SHARED{urlkeys}{$url};

$f$;

/*
create or replace function t2b (text) returns bytea language plperlu as $f$

my $t = shift;
$t =~ s!(\\|[^ -~])!sprintf("\\%03o",ord($1))!ge;
return $t;

$f$;

create or replace function b2t (bytea) returns text language plperlu as $f$

my $b = shift;
$b =~ s!\\(?:\\|(\d{3}))!$1 ? chr(oct($1)) : "\\"!ge; 
return $b;

$f$;

*/


create or replace function encrypt_url(data text, url text) returns text
language plpgsql as $f$

declare
	iv bytea;
begin
	-- can't use textsend becaue it translates to client encoding
    -- so use t2b
	iv := gen_random_bytes(15);
	return  encode(iv,'base64') || '|' ||
		encode(encrypt_iv(t2b(data),url_key(url),iv,'aes'),'base64');
end;

$f$;


-- the pure SQL decrypt_url function is quite a bit faster than plpgsql
create or replace function decrypt_url (data text, url text) returns text 
language sql immutable as $f$

  select b2t(decrypt_iv(decode(split_part($1,'|',2),'base64'),
                        url_key($2),
                        decode(split_part($1,'|',1),'base64'),
                        'aes'))

$f$;

alter table org_employees 
  alter column employee_number type text,
  alter column full_name type text
--  , add column plain_employee_number text
--  , add column plain_full_name text
  ;


create or replace function encrypt_org_emp() returns void language plpgsql 
as $$

declare
        rec record;

begin

        for rec in select url from merit_accounts order by url
        loop
--				continue when rec.url <> 'hradv';
			    raise NOTICE 'encrypting %.org_employees', rec.url;
                execute 'update ' ||
                        rec.url || '.org_employees ' ||
                        ' set employee_number =  ' || 
                             'encrypt_url(employee_number,$_$' || 
                             rec.url || '$_$), full_name = ' ||
                             'encrypt_url(full_name, $_$' || rec.url || '$_$)'
--							|| ' , plain_employee_number = employee_number '
--      						|| ' , plain_full_name =  full_name '
							 ;
        end loop;
end;

$$;

select encrypt_org_emp();

create or replace function replace_emp_num_idx() returns void language plpgsql
as $$

declare
        rec record;

begin

        for rec in select url from merit_accounts order by url
        loop
                            raise NOTICE 'adjusting indexes for %.org_employees', rec.url;
		execute 'drop index if exists ' || rec.url || '.' || rec.url || '_org_employees_pk';
		execute 'drop index if exists ' || rec.url || '.' || rec.url || '_org_employees_employee_number_idx';
		execute 'create unique index ' || rec.url || '_org_employees_pk on ' ||
			rec.url || '.' || 'org_employees (decrypt_url(employee_number,$_$' || rec.url || '$_$)) ' ||
			' with (fillfactor=80) tablespace "indexspace" ';
        end loop;
end;

$$;

select replace_emp_num_idx();

drop trigger if exists org_employees_audit_tr on org_employees;

create or replace function drop_emp_table_trigger() returns void language plpgsql
as $$

declare
        rec record;

begin

        for rec in select url from merit_accounts order by url
        loop
                            raise NOTICE 'removing trigger for %.org_employees', rec.url;
		execute 'drop trigger if exists ' || rec.url || '_org_employees_audit_tr on ' || 
                        rec.url || '.' || 'org_employees';
        end loop;
end;

$$;

select drop_emp_table_trigger();

