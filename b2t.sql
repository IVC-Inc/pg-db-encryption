
SET search_path = public;

CREATE OR REPLACE FUNCTION b2t(bytea) RETURNS text
     AS '$libdir/b2t'
     LANGUAGE C STRICT;

CREATE OR REPLACE FUNCTION t2b(text) RETURNS bytea
     AS '$libdir/b2t'
     LANGUAGE C STRICT;
