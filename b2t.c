
#include "postgres.h"
#include "fmgr.h"
#include "mb/pg_wchar.h"


#ifdef PG_MODULE_MAGIC
PG_MODULE_MAGIC;
#endif

PG_FUNCTION_INFO_V1(b2t);
PG_FUNCTION_INFO_V1(t2b);

Datum b2t(PG_FUNCTION_ARGS);
Datum t2b(PG_FUNCTION_ARGS);

Datum
b2t(PG_FUNCTION_ARGS)
{
    bytea     *b = PG_GETARG_BYTEA_P(0);
	text      *t;

	pg_verifymbstr(VARDATA(b),VARSIZE(b) - VARHDRSZ,false);

	t = (text *) palloc(VARSIZE(b));

#if PG_VERSION_NUM < 80300 
	VARATT_SIZEP(t) = VARSIZE(b);
#else
    SET_VARSIZE(t, VARSIZE(b));
#endif

	memcpy((void *) VARDATA(t),
           (void *) VARDATA(b), 
           VARSIZE(b) - VARHDRSZ); 
    PG_RETURN_TEXT_P(t);

}


Datum
t2b(PG_FUNCTION_ARGS)
{
  text     *t = PG_GETARG_TEXT_P_COPY(0);
  PG_RETURN_BYTEA_P((bytea *)t);

}
